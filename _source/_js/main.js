$('.profile .profile-content').waypoint(function(direction){
    
    $(this.element).parent('.profile').toggleClass('show');
    
    var size = $(window).width();
    var text = $(this.element).children('.profile-header');
    var image = $(this.element).prev();
    
    if ( size < 900 ) {
        mobileScroll();
    }
    
    $(window).resize(function() {
        var size = $(window).width();
        
        if ( size < 900 ) {
            mobileScroll();
        }
        
        if ( size >= 900 && $('body').hasClass('mobile-scroll') )
        {
            resetImage();
        }
        
    });
    
    
    function mobileScroll() {
        var imageHeight = image.height(); 
        var reference = imageHeight + 30;
        
        $('body').addClass('mobile-scroll');
        
        $(window).on('scroll', function(){
            var scrollTop = $(window).scrollTop();
            var textTop = text.offset().top;
            var distance = textTop - scrollTop;
            
            if ( distance <= reference )
            {
                if ( $('body').hasClass('mobile-scroll') )
                {
                    $(image).addClass('fixed').css('position', 'absolute').css({'top':'calc(100vh - ' + reference +'px)'});
                }
            }
            
            if ( distance > reference && $(image).hasClass('fixed') )
            {
                $(image).removeClass('fixed').css({'position':'', 'top': ''});
            }
            
            
        });
    }
    
    function resetImage() {
        $(image).removeClass('fixed').css({'position':'', 'top': ''});
        $('body').removeClass('mobile-scroll');
    }
    
}, {offset: '75%'});


$('.profile .profile-header').waypoint(function(direction){
    
    $(this.element).parent().parent('.profile').toggleClass('show');
    
});


// Color change on line

$('#maura-farrell, #jessica-rosenbaum, #vivian-english, #jake-thurman').waypoint(function(direction){
    if ( direction === 'down' )
    {
        $('#line').css('backgroundColor', '#DF542A');
    }
    
    else {
        $('#line').css('backgroundColor', '#9A6BB8');
    }
    
}, {offset: '25%'});

$('#matt-lai, #susanna-jones, #damian-jones').waypoint(function(direction){
    
    if ( direction === 'down' )
    {
        $('#line').css('backgroundColor', '#9A6BB8');
    }
    
    else {
        $('#line').css('backgroundColor', '#DF542A');
    }
}, {offset: '25%'});


function waveSurfer() {
    
    function secondsTimeSpanToHMS(s) {
        var m = Math.floor(s/60);
        s -= m*60;
        return (m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s);
    }
    
    var damianJones = WaveSurfer.create({
       container: '#damian-jones-audio .wave-container',
       progressColor: '#DF542A',
       waveColor: '#9B9B9B',
       height:40,
       barWidth: 1,
       barHeight:4,
       cursorColor: '#fff',
       scrollParent: true
    });
    
    damianJones.load('/audio/interview1.mp3');
    
    damianJones.on('ready', function() {
        
        var duration = damianJones.getDuration().toFixed(0);
        var time = secondsTimeSpanToHMS(duration);
                
        $('#damian-jones-audio .play').on('click', function(){
            damianJones.playPause();
        });
        
        $('#damian-jones-audio .duration').text(time);
       
        damianJones.on('play', function(){
            $('#damian-jones-audio .play').addClass('playing');
        });
        
        damianJones.on('pause', function(){
            $('#damian-jones-audio .play').removeClass('playing');
        });
       
        
    });
    
    damianJones.on('audioprocess', function(){
       var current = damianJones.getCurrentTime().toFixed(0);
       var time = secondsTimeSpanToHMS(current);
       $('#damian-jones-audio .duration').text(time);
    });
    
    
    var mauraFarrell = WaveSurfer.create({
       container: '#maura-farrell-audio .wave-container',
       progressColor: '#DF542A',
       waveColor: '#9B9B9B',
       height:40,
       barWidth: 1,
       barHeight:4,
       cursorColor: '#fff',
       scrollParent: true
    });
    
    mauraFarrell.load('/audio/interview2.mp3');
    
    mauraFarrell.on('ready', function() {
        
        var duration = mauraFarrell.getDuration().toFixed(0);
        var time = secondsTimeSpanToHMS(duration);
        
        $('#maura-farrell-audio .play').on('click', function(){
            mauraFarrell.playPause();
        });
        
        $('#maura-farrell-audio .duration').text(time);
       
        mauraFarrell.on('play', function(){
            $('#maura-farrell-audio .play').addClass('playing');
        });
        
        mauraFarrell.on('pause', function(){
            $('#maura-farrell-audio .play').removeClass('playing');
        });
       
        
    });
    
    mauraFarrell.on('audioprocess', function(){
       var current = mauraFarrell.getCurrentTime().toFixed(0);
       var time = secondsTimeSpanToHMS(current);
       $('#maura-farrell-audio .duration').text(time);
    });
    
    var jakeThurman = WaveSurfer.create({
       container: '#jake-thurman-audio .wave-container',
       progressColor: '#DF542A',
       waveColor: '#9B9B9B',
       height:40,
       barWidth: 1,
       barHeight:4,
       cursorColor: '#fff',
       scrollParent: true
    });
    
    jakeThurman.load('/audio/interview3.mp3');
    
    jakeThurman.on('ready', function() {
        
        var duration = jakeThurman.getDuration().toFixed(0);
        var time = secondsTimeSpanToHMS(duration);
        
        $('#jake-thurman-audio .play').on('click', function(){
            jakeThurman.playPause();
        });
        
        $('#jake-thurman-audio .duration').text(time);
       
        jakeThurman.on('play', function(){
            $('#jake-thurman-audio .play').addClass('playing');
        });
        
        jakeThurman.on('pause', function(){
            $('#jake-thurman-audio .play').removeClass('playing');
        });
       
        
    });
    
    jakeThurman.on('audioprocess', function(){
       var current = jakeThurman.getCurrentTime().toFixed(0);
       var time = secondsTimeSpanToHMS(current);
       $('#jake-thurman-audio .duration').text(time);
    });
    
    var mattLai = WaveSurfer.create({
       container: '#matt-lai-audio .wave-container',
       progressColor: '#DF542A',
       waveColor: '#9B9B9B',
       height:40,
       barWidth: 1,
       barHeight:4,
       cursorColor: '#fff',
       scrollParent: true
    });
    
    mattLai.load('/audio/interview4.mp3');
    
    mattLai.on('ready', function() {
        
        var duration = mattLai.getDuration().toFixed(0);
        var time = secondsTimeSpanToHMS(duration);
        
        $('#matt-lai-audio .play').on('click', function(){
            mattLai.playPause();
        });
        
        $('#matt-lai-audio .duration').text(time);
       
        mattLai.on('play', function(){
            $('#matt-lai-audio .play').addClass('playing');
        });
        
        mattLai.on('pause', function(){
            $('#matt-lai-audio .play').removeClass('playing');
        });
       
        
    });
    
    mattLai.on('audioprocess', function(){
       var current = mattLai.getCurrentTime().toFixed(0);
       var time = secondsTimeSpanToHMS(current);
       $('#matt-lai-audio .duration').text(time);
    });
    
    var crissyCaceres = WaveSurfer.create({
       container: '#crissy-caceres-audio .wave-container',
       progressColor: '#DF542A',
       waveColor: '#9B9B9B',
       height:40,
       barWidth: 1,
       barHeight:4,
       cursorColor: '#fff',
       scrollParent: true
    });
    
    crissyCaceres.load('/audio/interview5.mp3');
    
    crissyCaceres.on('ready', function() {
        
        var duration = crissyCaceres.getDuration().toFixed(0);
        var time = secondsTimeSpanToHMS(duration);
                
        $('#crissy-caceres-audio .play').on('click', function(){
            crissyCaceres.playPause();
        });
        
        $('#crissy-caceres-audio .duration').text(time);
       
        crissyCaceres.on('play', function(){
            $('#crissy-caceres-audio .play').addClass('playing');
        });
        
        crissyCaceres.on('pause', function(){
            $('#crissy-caceres-audio .play').removeClass('playing');
        });
       
        
    });
    
    crissyCaceres.on('audioprocess', function(){
       var current = crissyCaceres.getCurrentTime().toFixed(0);
       var time = secondsTimeSpanToHMS(current);
       $('#crissy-caceres-audio .duration').text(time);
    });
    
    var jessicaRosenbaum = WaveSurfer.create({
       container: '#jessica-rosenbaum-audio .wave-container',
       progressColor: '#DF542A',
       waveColor: '#9B9B9B',
       height:40,
       barWidth: 1,
       barHeight:4,
       cursorColor: '#fff',
       scrollParent: true
    });
    
    jessicaRosenbaum.load('/audio/interview6.mp3');
    
    jessicaRosenbaum.on('ready', function() {
        
        var duration = jessicaRosenbaum.getDuration().toFixed(0);
        var time = secondsTimeSpanToHMS(duration);
                
        $('#jessica-rosenbaum-audio .play').on('click', function(){
            jessicaRosenbaum.playPause();
        });
        
        $('#jessica-rosenbaum-audio .duration').text(time);
       
        jessicaRosenbaum.on('play', function(){
            $('#jessica-rosenbaum-audio .play').addClass('playing');
        });
        
        jessicaRosenbaum.on('pause', function(){
            $('#jessica-rosenbaum-audio .play').removeClass('playing');
        });
       
        
    });
    
    jessicaRosenbaum.on('audioprocess', function(){
       var current = jessicaRosenbaum.getCurrentTime().toFixed(0);
       var time = secondsTimeSpanToHMS(current);
       $('#jessica-rosenbaum-audio .duration').text(time);
    });
    
    var susannaJones = WaveSurfer.create({
       container: '#susanna-jones-audio .wave-container',
       progressColor: '#DF542A',
       waveColor: '#9B9B9B',
       height:40,
       barWidth: 1,
       barHeight:4,
       cursorColor: '#fff',
       scrollParent: true
    });
    
    susannaJones.load('/audio/interview7.mp3');
    
    susannaJones.on('ready', function() {
        
        var duration = susannaJones.getDuration().toFixed(0);
        var time = secondsTimeSpanToHMS(duration);
        
        $('#susanna-jones-audio .play').on('click', function(){
            susannaJones.playPause();
        });
        
        $('#susanna-jones-audio .duration').text(time);
       
        susannaJones.on('play', function(){
            $('#susanna-jones-audio .play').addClass('playing');
        });
        
        susannaJones.on('pause', function(){
            $('#susanna-jones-audio .play').removeClass('playing');
        });
       
        
    });
    
    susannaJones.on('audioprocess', function(){
       var current = susannaJones.getCurrentTime().toFixed(0);
       var time = secondsTimeSpanToHMS(current);
       $('#susanna-jones-audio .duration').text(time);
    });
    
    var vivianEnglish = WaveSurfer.create({
       container: '#vivian-english-audio .wave-container',
       progressColor: '#DF542A',
       waveColor: '#9B9B9B',
       height:40,
       barWidth: 1,
       barHeight:4,
       cursorColor: '#fff',
       scrollParent: true
    });
    
    vivianEnglish.load('/audio/interview8.mp3');
    
    vivianEnglish.on('ready', function() {
        
        var duration = vivianEnglish.getDuration().toFixed(0);
        var time = secondsTimeSpanToHMS(duration);
        
        $('#vivian-english-audio .play').on('click', function(){
            vivianEnglish.playPause();
        });
        
        $('#vivian-english-audio .duration').text(time);
       
        vivianEnglish.on('play', function(){
            $('#vivian-english-audio .play').addClass('playing');
        });
        
        vivianEnglish.on('pause', function(){
            $('#vivian-english-audio .play').removeClass('playing');
        });
       
        
    });
    
    vivianEnglish.on('audioprocess', function(){
       var current = vivianEnglish.getCurrentTime().toFixed(0);
       var time = secondsTimeSpanToHMS(current);
       $('#vivian-english-audio .duration').text(time);
    });
}
waveSurfer();

function videoModal(){
  

  $('.video-gallery a').on('click', function(e){
    var embedCode = $(this).attr('data-embed'), 
      videoEmbed = '<div class="video-container"><div class="close-btn">Close<span></span><span></span></div><div class="video"><iframe width="560" height="315" src="https://www.youtube.com/embed/' + embedCode + '?rel=0&amp;showinfo=0;autoplay=1;" frameborder="0" allowfullscreen></iframe></div></div>',
      modal = '<div id="video-modal">' + videoEmbed  + '</div>';
      
      (e).preventDefault();
      $('body').append(modal);

      $('.close-btn').on('click', function(){
        $('#video-modal').remove();
      });
  });

  
}


function naisResize ( $, window ) {
  window.watchResize = function( callback )
  {
    var resizing;
    function done()
    {
      clearTimeout( resizing );
      resizing = null;
      callback();
    }
    $(window).resize(function(){
      if ( resizing )
      {
        clearTimeout( resizing );
        resizing = null;
      }
      resizing = setTimeout( done, 50 );
    });
    // init
    callback();
  };
  window.watchResize(function(){
    var size = $(window).width();

    if ( size >= 600  && !$('body').hasClass('video-modal-enabled')) {
      $('body').addClass('video-modal-enabled');
      videoModal();
    }

    if (size < 600 && $('body').hasClass('video-modal-enabled'))
    {
      $('body').removeClass('video-modal-enabled');
      $('.video').unbind('click');
    }

  });
}
naisResize( jQuery, window );
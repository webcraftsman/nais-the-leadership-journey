# Live Site #

You can view the live site for this repository at http://leadership.annualconference.nais.org/

# Functionality #

This site uses [Waypoints](http://imakewebthings.com/waypoints) and [wavesurfer.js](https://wavesurfer-js.org) jQuery plugins for functionality.
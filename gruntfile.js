module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			dist: {
				options: {
					style: 'compressed'
				},
				files: {
					'_css/main.css' : '_source/_scss/main.scss'
				}
			}
		},
		concat: {   
			dist: {
				src: ['_source/_js/main.js'],
				dest: '_js/main.js'
			}
		},
		uglify: {
			build: {
				src: '_js/main.js',
				dest: '_js/main.min.js'
			}
		},
		watch: {
			css: {
				files: '**/*.scss',
				tasks: ['sass']
			},
			scripts: {
				files:'_source/_js/*.js',
				tasks:['concat','uglify']
			}
		}
	});
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	grunt.registerTask('default',['concat','uglify']);
}